using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CMSL.Databases.GDE.Tests.CrossCutting;

public interface IInnerDependencyInjection
{
    IServiceScopeFactory ServiceScopeFactory { get; }
    IConfiguration Configuration { get; }
    IServiceScope CreateScope();
}