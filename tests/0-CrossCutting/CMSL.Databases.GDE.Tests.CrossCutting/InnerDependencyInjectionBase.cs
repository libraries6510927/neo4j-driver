using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CMSL.Databases.GDE.Tests.CrossCutting;

public class InnerDependencyInjectionBase: IInnerDependencyInjection, IDisposable
{
    private readonly IServiceProvider _globalServiceProvider;
    
    public IServiceScopeFactory ServiceScopeFactory { get; }
    public IConfiguration Configuration { get; }

    protected InnerDependencyInjectionBase(Action<IServiceCollection, IConfiguration>? additionalServices = null, Action<IConfigurationBuilder>? additionalConfiguration = null)
    {
        var baseDir = Path.GetDirectoryName(GetType().Assembly.Location) ?? throw new InvalidOperationException("Unable to get assembly directory");
        
        var configuration = new ConfigurationBuilder();
        configuration.SetBasePath(baseDir);
        if (additionalConfiguration is null) ConfigureTestsConfiguration(configuration);
        else additionalConfiguration.Invoke(configuration);
        Configuration = configuration.Build();

        var serviceCollection = new ServiceCollection()
            .AddSingleton<IConfiguration>(_ => Configuration);
        additionalServices?.Invoke(serviceCollection, Configuration);

        _globalServiceProvider = serviceCollection.BuildServiceProvider();
        
        ServiceScopeFactory = _globalServiceProvider.GetRequiredService<IServiceScopeFactory>();
    }

    private static void ConfigureTestsConfiguration(IConfigurationBuilder configurationBuilder)
    {
        configurationBuilder
            .AddEnvironmentVariables();
    }

    public IServiceScope CreateScope() => ServiceScopeFactory.CreateScope();

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    private void Dispose(bool isDisposing) => DisposeAsync()
        .GetAwaiter()
        .GetResult();
    
    protected  virtual async ValueTask DisposeAsync()
    {
        if (_globalServiceProvider is IAsyncDisposable disposableServiceProvider) await disposableServiceProvider.DisposeAsync();
    }
}