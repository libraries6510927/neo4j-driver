namespace CMSL.Databases.GDE.Tests.Integration.Neo4j;

public class TestModel
{
    public string Name { get; set; }
    public int Age { get; set; }

    public bool IsOverAge => Age >= 18;
}