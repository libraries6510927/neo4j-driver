using CMSL.Databases.GDE.Drivers.Neo4j;
using CMSL.Databases.GDE.Tests.CrossCutting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Neo4j.Driver;

namespace CMSL.Databases.GDE.Tests.Integration.Neo4j.Base;

public class DatabaseCleaner : InnerDependencyInjectionBase
{
    public DatabaseCleaner() : base(ConfigureServices, ConfigureConfiguration)
    { 
        CleanDatabase().GetAwaiter().GetResult();
    }

    private async Task CleanDatabase()
    {
        using var scope = CreateScope();
        var neo4jDriver = scope.ServiceProvider.GetRequiredService<IDriver>();
        await using var session = neo4jDriver.AsyncSession();
        await session.ExecuteWriteAsync(async queryRunner => { await queryRunner.RunAsync("MATCH (n) DETACH DELETE n"); });
    }

    private static void ConfigureConfiguration(IConfigurationBuilder cfg)
    {
        cfg.AddJsonFile("appsettings.neo4jtests.json");
    }

    private static void ConfigureServices(IServiceCollection sc, IConfiguration cfg)
    {
        sc
            .AddNeo4j(cfg);
    }
}