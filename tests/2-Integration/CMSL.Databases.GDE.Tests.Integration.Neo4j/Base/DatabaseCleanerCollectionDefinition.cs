namespace CMSL.Databases.GDE.Tests.Integration.Neo4j.Base;

[CollectionDefinition(CollectionName)]
public class DatabaseCleanerCollectionDefinition: ICollectionFixture<DatabaseCleaner>
{
    public const string CollectionName = "DatabaseCleaner";
}