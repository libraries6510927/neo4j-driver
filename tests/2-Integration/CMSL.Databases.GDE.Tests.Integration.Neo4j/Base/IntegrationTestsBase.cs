using CMSL.Databases.GDE.Drivers.Neo4j;
using CMSL.Databases.GDE.Tests.CrossCutting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CMSL.Databases.GDE.Tests.Integration.Neo4j.Base;

[Collection(DatabaseCleanerCollectionDefinition.CollectionName)]
public class IntegrationTestsBase : InnerDependencyInjectionBase
{
    public IntegrationTestsBase(): base(ConfigureServices, ConfigureConfiguration)
    {
        
    }

    private static void ConfigureServices(IServiceCollection sc, IConfiguration configuration)
    {
        sc
            .AddNeo4j(configuration);
    }

    private static void ConfigureConfiguration(IConfigurationBuilder cfg)
    {
        cfg.AddJsonFile("appsettings.neo4jtests.json");
    }
    
}