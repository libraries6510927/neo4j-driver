using CMSL.Databases.GDE.Abstractions;
using CMSL.Databases.GDE.Tests.Integration.Neo4j.Base;
using Microsoft.Extensions.DependencyInjection;

namespace CMSL.Databases.GDE.Tests.Integration.Neo4j;

public class NodesRepositoryTests : IntegrationTestsBase
{
    [Fact]
    public async Task AddAsyncShouldAddNodeToDatabaseGivenValidRequest()
    {
        var modelToAdd01 = new TestModel
        {
            Age = 19,
            Name = "Test 01"
        };
        var modelToAdd02 = new TestModel
        {
            Age = 27,
            Name = "Added by the Neo4j Driver"
        };

        var modelsToAdd = new[] { modelToAdd01, modelToAdd02 };

        using (var scope = CreateScope())
        {
            var sut = scope.ServiceProvider.GetRequiredService<IGraphDatabaseContext>();
            var nodeRepository = sut.GetNodesRepository<TestModel>();

            foreach (var model in modelsToAdd) await nodeRepository.Add(model);
        }

        using (var scope = CreateScope())
        {
            var sut = scope.ServiceProvider.GetRequiredService<IGraphDatabaseContext>();

            foreach (var model in modelsToAdd)
            {
                var nodeRepository = sut.GetNodesRepository<TestModel>();
                var name = model.Name;

                var modelInDatabase = await nodeRepository
                    .Where(x => x.Age == model.Age)
                    .Where(x => x.Name == name)
                    .SingleAsync();

                Assert.Equal(model.Age, modelInDatabase.Age);
                Assert.Equal(model.Name, modelInDatabase.Name);
            }
        }
    }

    [Fact]
    public async Task SingleAsyncShouldApplyFiltersGivenValidExpressions()
    {
        const int ageToSelect = 34;
        const string nameToSelect = "Test 02";

        using (var scope = CreateScope())
        {
            var modelToAdd01 = new TestModel
            {
                Age = 19,
                Name = "Test 01"
            };
            var modelToAdd02 = new TestModel
            {
                Age = ageToSelect,
                Name = "Added by the Neo4j Driver"
            };
            var modelToAdd03 = new TestModel
            {
                Age = ageToSelect,
                Name = nameToSelect
            };


            var sut = scope.ServiceProvider.GetRequiredService<IGraphDatabaseContext>();
            var nodeRepository = sut.GetNodesRepository<TestModel>();

            await nodeRepository.Add(modelToAdd01);
            await nodeRepository.Add(modelToAdd02);
            await nodeRepository.Add(modelToAdd03);
        }

        using (var scope = CreateScope())
        {
            var sut = scope.ServiceProvider.GetRequiredService<IGraphDatabaseContext>();
            var nodeRepository = sut.GetNodesRepository<TestModel>();

            var node = await nodeRepository
                .Where(x => x.Age == ageToSelect)
                .Where(x => x.Name == nameToSelect)
                .SingleAsync();

            Assert.Equal(ageToSelect, node.Age);
            Assert.Equal(nameToSelect, node.Name);
        }
    }

    [Fact]
    public async Task ToListAsyncShouldReturnExpectedNodesGivenValidQuery()
    {
        const int ageToSelect = 40;

        using (var scope = CreateScope())
        {
            var modelToAdd01 = new TestModel
            {
                Age = 19,
                Name = Guid.NewGuid().ToString()
            };
            var modelToAdd02 = new TestModel
            {
                Age = 48,
                Name = Guid.NewGuid().ToString()
            };
            var modelToAdd03 = new TestModel
            {
                Age = 40,
                Name = Guid.NewGuid().ToString()
            };

            var sut = scope.ServiceProvider.GetRequiredService<IGraphDatabaseContext>();
            var nodeRepository = sut.GetNodesRepository<TestModel>();

            await nodeRepository.Add(modelToAdd01);
            await nodeRepository.Add(modelToAdd02);
            await nodeRepository.Add(modelToAdd03);
        }

        using (var scope = CreateScope())
        {
            var sut = scope.ServiceProvider.GetRequiredService<IGraphDatabaseContext>();
            var nodeRepository = sut.GetNodesRepository<TestModel>();

            var nodes = await nodeRepository
                .Where(x => x.Age >= ageToSelect)
                .ToListAsync();

            Assert.Equal(2, nodes.Count);
            nodes.ForEach(x => Assert.True(x.Age >= ageToSelect));
        }
    }

    [Fact]
    public async Task DeleteAsyncShouldReturnNodesGivenValidFilters()
    {
        const string nameToSelect = "Delete me";
        const string nameToRemain = "Not deleted";

        using (var scope = CreateScope())
        {
            var modelToAdd01 = new TestModel
            {
                Age = 88,
                Name = nameToRemain
            };
            var modelToAdd02 = new TestModel
            {
                Age = 48,
                Name = nameToSelect
            };
            var modelToAdd03 = new TestModel
            {
                Age = 40,
                Name = nameToSelect
            };

            var sut = scope.ServiceProvider.GetRequiredService<IGraphDatabaseContext>();
            var nodeRepository = sut.GetNodesRepository<TestModel>();

            await nodeRepository.Add(modelToAdd01);
            await nodeRepository.Add(modelToAdd02);
            await nodeRepository.Add(modelToAdd03);
        }

        using (var scope = CreateScope())
        {
            var sut = scope.ServiceProvider.GetRequiredService<IGraphDatabaseContext>();
            var nodeRepository = sut.GetNodesRepository<TestModel>();

            await nodeRepository
                .Where(x => x.Name == nameToSelect)
                .DeleteAsync();
        }

        using (var scope = CreateScope())
        {
            var sut = scope.ServiceProvider.GetRequiredService<IGraphDatabaseContext>();

            var nodes = await sut.GetNodesRepository<TestModel>()
                .Where(x => x.Name == nameToSelect)
                .ToListAsync();

            Assert.Empty(nodes);

            var notDeleted = await sut.GetNodesRepository<TestModel>()
                .Where(x => x.Name == nameToRemain)
                .SingleAsync();

            Assert.NotNull(notDeleted);
        }
    }
}