using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting;
using CMSL.Databases.GDE.Abstractions.Exceptions;
using CMSL.Databases.GDE.Abstractions.QueryBuilding;
using CMSL.Databases.GDE.Abstractions.Repositories;
using CMSL.Databases.GDE.Repositories;
using CMSL.Databases.GDE.Tests.CrossCutting;
using CMSL.Databases.GDE.Tests.Unit.Extensions;
using CMSL.Databases.GDE.Tests.Unit.TestModels;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CMSL.Databases.GDE.Tests.Unit.Repositories;

public class NodeRepositoriesTests: InnerDependencyInjectionBase
{
    public NodeRepositoriesTests(): base(ConfigureServices)
    {
        
    }

    private static void ConfigureServices(IServiceCollection sc, IConfiguration cfg)
    {
        sc
            .AddGraphDatabaseBase()
            .AddScopedMock<IQueryDtoEvaluator>()
            .AddScopedMock<IQueryBuilder>()
            .AddScopedMock<IGraphDatabaseDriver>();
    }

    [Fact]
    public async Task DeleteShouldThrowExceptionGivenEmptyFilters()
    {
        using var scope = CreateScope();
        var sut = scope.ServiceProvider.GetRequiredService<INodeRepository<OnlyOneAllowedPropertyTestModel>>();

        await Assert.ThrowsAnyAsync<GdeException>(async () => await sut.DeleteAsync());
    }
}