using System.Collections.ObjectModel;
using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.ObjectPropertiesExplorer;
using CMSL.Databases.GDE.DotNetSyntaxInterpreting.ObjectPropertiesExplorer;
using CMSL.Databases.GDE.DotNetSyntaxInterpreting.ObjectPropertiesExplorer.Exceptions;
using CMSL.Databases.GDE.Tests.CrossCutting;
using CMSL.Databases.GDE.Tests.Unit.TestModels;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CMSL.Databases.GDE.Tests.Unit.DotNetSyntaxInterpreting.ObjectsPropertiesExplorer;

public class DatabasePropertiesBinderTests : InnerDependencyInjectionBase
{
    public DatabasePropertiesBinderTests(): base(ConfigureServices)
    {
        
    }

    private static void ConfigureServices(IServiceCollection serviceCollection, IConfiguration configuration)
    {
        serviceCollection
            .AddSingleton<DatabasePropertiesBinder>()
            .AddScoped<IPropertyExplorer, PropertyExplorer>();
    }

    [Theory]
    [ClassData(typeof(PropertiesExpectedModelTestValues))]
    public void BindToShouldReturnExpectedObjectGivenValidPropertyValuePairs(IReadOnlyDictionary<string,object?> properties, RawPropertiesTestModel expectedModel)
    {
        using var scope = CreateScope();

        var sut = scope.ServiceProvider.GetRequiredService<DatabasePropertiesBinder>();
        var model = sut.BindTo<RawPropertiesTestModel>(properties);
        
        Assert.Equal(expectedModel, model);
    }

    private class PropertiesExpectedModelTestValues : TheoryData<IReadOnlyDictionary<string, object?>, RawPropertiesTestModel>
    {
        public PropertiesExpectedModelTestValues()
        {
            AddTest1();
            AddTest2();
            AddTest3();
        }

        private void AddTest3()
        {
            int? expectedProp10 = -98;
            const bool expectedProp11 = true;
            const char expectedProp05 = 'a';

            var properties = new Dictionary<string, object?>()
            {
                { "id", 800 },
                { "Another", "Test" },
                { "Prop10", expectedProp10 },
                { "Prop11", expectedProp11 },
                { "Prop05", expectedProp05 },
                {"Prop14", null}
            };

            var expectedModel = new RawPropertiesTestModel
            {
                Prop05 = expectedProp05,
                Prop10 = expectedProp10,
                Prop11 = expectedProp11,
                Prop14 = null
            };
            Add(new ReadOnlyDictionary<string, object?>(properties), expectedModel);
        }

        private void AddTest2()
        {
            int? expectedProp10 = null;
            const bool expectedProp11 = true;
            const string expectedProp13 = "     ";
            var expectedGuid = Guid.NewGuid().ToString();

            var properties = new Dictionary<string, object?>()
            {
                { "id", 3 },
                { "Prop10", expectedProp10 },
                { "Prop11", expectedProp11 },
                { "Prop13", expectedProp13 },
                {"Prop14", expectedGuid}
            };

            var expectedModel = new RawPropertiesTestModel
            {
                Prop10 = expectedProp10,
                Prop11 = expectedProp11,
                Prop13 = expectedProp13,
                Prop14 = Guid.Parse(expectedGuid)
            };
            
            Add(new ReadOnlyDictionary<string, object?>(properties), expectedModel);
        }

        private void AddTest1()
        {
            const string expectedProp01 = "Ahupa";
            const uint expectedProp03 = 7;
            const float expectedProp07 = -3.75f;
            var expectedProp09 = Guid.NewGuid().ToString();
            
            var properties = new Dictionary<string, object?>
            {
                {"Prop01", expectedProp01},
                {"Prop03", expectedProp03},
                {"Prop07", expectedProp07},
                {"Prop09", expectedProp09}
            };

            var expectedModel = new RawPropertiesTestModel
            {
                Prop01 = expectedProp01,
                Prop03 = expectedProp03,
                Prop07 = expectedProp07,
                Prop09 = Guid.Parse(expectedProp09)
            };
            
            Add(new ReadOnlyDictionary<string, object?>(properties), expectedModel);
        }
    }

    [Fact]
    public void BindToShouldThrowExceptionGivenWrongValueType()
    {
        var wrongProperty = new Dictionary<string, object?>
        {
            {"Prop01", 84}
        };

        using var scope = CreateScope();
        var sut = scope.ServiceProvider.GetRequiredService<DatabasePropertiesBinder>();
        Assert.Throws<WrongPropertyValueTypeException>(() => sut.BindTo<RawPropertiesTestModel>(new ReadOnlyDictionary<string, object?>(wrongProperty)));
    }

    [Fact]
    public void BindToShouldThrowGiveExceptionNullValueOnNonNullableProperty()
    {
        var wrongProperty = new Dictionary<string, object?>
        {
            {"Prop03", null}
        };

        using var scope = CreateScope();
        var sut = scope.ServiceProvider.GetRequiredService<DatabasePropertiesBinder>();
        Assert.Throws<NullValueOnNotNullablePropertyException>(() => sut.BindTo<RawPropertiesTestModel>(new ReadOnlyDictionary<string, object?>(wrongProperty)));
    }
}