using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting;
using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.LambdaSyntax;
using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.ObjectPropertiesExplorer;
using CMSL.Databases.GDE.DotNetSyntaxInterpreting.ObjectPropertiesExplorer;
using CMSL.Databases.GDE.Languages.Cypher;
using CMSL.Databases.GDE.Languages.Cypher.DotNetSyntaxInterpreting;
using CMSL.Databases.GDE.Tests.CrossCutting;
using CMSL.Databases.GDE.Tests.Unit.TestModels;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CMSL.Databases.GDE.Tests.Unit.DotNetSyntaxInterpreting;

public class CypherPropertyEvaluatorTests: InnerDependencyInjectionBase
{
    public CypherPropertyEvaluatorTests(): base(AddServices)
    {
        
    }

    private static void AddServices(IServiceCollection serviceCollection, IConfiguration configuration)
    {
        serviceCollection
            .AddSingleton<IPropertyExplorer, PropertyExplorer>()
            .AddSingleton<IQueryDtoEvaluator, CypherQueryDtoEvaluator>();
    }


    [Theory]
    [ClassData(typeof(PropertyMappingsValues))]
    public void EvaluateShouldReturnExpectedKeyValuePairsGivenTypeWithAllowedProperties(object model, IDictionary<string, string> expectedValues)
    {
        using var scope = CreateScope();

        var explorer = scope.ServiceProvider.GetRequiredService<IPropertyExplorer>();
        var properties = explorer.GetProperties(model);

        var evaluator = scope.ServiceProvider.GetRequiredService<IQueryDtoEvaluator>();
        var values = evaluator.Evaluate(properties);
        
        Assert.Equal(expectedValues.Count, values.Count);
        foreach (var value in values)
        {
            Assert.True(expectedValues.ContainsKey(value.Key));
            Assert.Equal(expectedValues[value.Key], value.Value);
        }
    }

    [Fact]
    public void EvaluateShouldReturnEmptyDictionaryGivenEmptyModel()
    {
        using var scope = CreateScope();

        var explorer = scope.ServiceProvider.GetRequiredService<IPropertyExplorer>();
        var properties = explorer.GetProperties(new{});

        var evaluator = scope.ServiceProvider.GetRequiredService<IQueryDtoEvaluator>();
        var values = evaluator.Evaluate(properties);
        
        Assert.Empty(values);
    }

    private class PropertyMappingsValues : TheoryData<object, IDictionary<string, string>>
    {
        public PropertyMappingsValues()
        {
            AddRawPropertiesTest();
            AddOnlyOneAllowedPropertyTest();
        }

        private void AddOnlyOneAllowedPropertyTest()
        {
            var test = new OnlyOneAllowedPropertyTestModel
            {
                Allowed = "Ok"
            };

            var expectedReturns = new Dictionary<string, string>()
            {
                {"Allowed", "'Ok'"}
            };
            
            Add(test, expectedReturns);
        }

        private void AddRawPropertiesTest()
        {
            var guid = Guid.NewGuid();
            var test = new RawPropertiesTestModel
            {
                Prop01 = "Test",
                Prop02 = -69,
                Prop03 = 38,
                Prop04 = true,
                Prop05 = 'c',
                Prop06 = 8.57d,
                Prop07 = -6.05f,
                Prop08 = .37m,
                Prop09 = guid,
                Prop11 = false,
                Prop12 = string.Empty,
                Prop13 = " "
            };

            var expectedMappings = new Dictionary<string, string>()
            {
                { "Prop01", "'Test'" },
                { "Prop02", "-69" },
                { "Prop03", "38" },
                { "Prop04", "true" },
                { "Prop05", "'c'" },
                { "Prop06", "8.57" },
                { "Prop07", "-6.05" },
                { "Prop08", "0.37" },
                { "Prop09", $"'{guid}'" },
                { "Prop10", "NULL" },
                { "Prop11", "false" },
                { "Prop12", "''" },
                { "Prop13", "' '" },
                {"Prop14", $"'{Guid.Empty}'"}
            };

            Add(test, expectedMappings);
        }
    }

    [Theory]
    [ClassData(typeof(QueryExpressionData))]
    public void EvaluateShouldReturnExpectedExpressionsGivenValidQuery(QueryExpression expression, string expectedPropertyName,  string expectedExpression, string expectedValue)
    {
        using var scope = CreateScope();
        var sut = scope.ServiceProvider.GetRequiredService<IQueryDtoEvaluator>();

        var result = sut.Evaluate(new QueryExpressions(new[] {expression}));
        var evaluatedExpression = result.Expressions.Single();
        
        Assert.Equal(expectedPropertyName, evaluatedExpression.PropertyName);
        Assert.Equal(expectedExpression, evaluatedExpression.ExpressionOperator);
        Assert.Equal(expectedValue, evaluatedExpression.PropertyValue);
    }
    
    private class QueryExpressionData: TheoryData<QueryExpression, string, string, string>
    {
        public QueryExpressionData()
        {
            Add(new QueryExpression(QueryExpressionType.DifferentOf, "test01", 3), "test01", "<>", "3");
            Add(new QueryExpression(QueryExpressionType.EqualTo, "test02", true), "test02", "=", "true");
            Add(new QueryExpression(QueryExpressionType.GreaterThan, "test03", -.57f), "test03", ">", "-0.57");
            Add(new QueryExpression(QueryExpressionType.GreaterOrEqualTo, "test04", "abc"), "test04", ">=", "'abc'");
            Add(new QueryExpression(QueryExpressionType.LowerThan, "test05", 80), "test05", "<", "80");
            Add(new QueryExpression(QueryExpressionType.LowerOrEqualTo, "test06", 'c'), "test06", "<=", "'c'");
            Add(new QueryExpression(QueryExpressionType.Is, "test07", null), "test07", "IS", "NULL");
            Add(new QueryExpression(QueryExpressionType.IsNot, "test08", null), "test08", "IS NOT", "NULL");
        }
    }

}