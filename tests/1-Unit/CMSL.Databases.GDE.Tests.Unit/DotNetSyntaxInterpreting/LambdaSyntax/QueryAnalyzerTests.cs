using System.Linq.Expressions;
using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.LambdaSyntax;
using CMSL.Databases.GDE.DotNetSyntaxInterpreting.LambdaSyntax;
using CMSL.Databases.GDE.DotNetSyntaxInterpreting.LambdaSyntax.Exceptions;
using CMSL.Databases.GDE.DotNetSyntaxInterpreting.LambdaSyntax.ExpressionAnalyzers;
using CMSL.Databases.GDE.Tests.CrossCutting;
using CMSL.Databases.GDE.Tests.Unit.TestModels;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CMSL.Databases.GDE.Tests.Unit.DotNetSyntaxInterpreting.LambdaSyntax;

public class QueryAnalyzerTests : InnerDependencyInjectionBase
{
    public QueryAnalyzerTests() : base(ConfigureServices)
    {
    }

    private static void ConfigureServices(IServiceCollection serviceCollection, IConfiguration configuration)
    {
        serviceCollection
            .AddTransient<IQueryAnalyzer, QueryAnalyzer>()
            .AddSingleton<IExpressionAnalyzerFactory, ExpressionAnalyzerFactory>();
    }

    [Theory]
    [ClassData(typeof(SuccessTestData))]
    public void AnalyzeQueryShouldReturnExpectedQueryExpressionGivenValidQuery(
        Expression<Func<RawPropertiesTestModel, bool>> query,
        string expectedPropertyName, 
        QueryExpressionType expectedQueryExpressionType,
        Func<object?, bool> equalityValueCallback
    )
    {
        using var scope = CreateScope();

        var sut = scope.ServiceProvider.GetRequiredService<IQueryAnalyzer>();
        var result = sut.AnalyzeQuery(query);

        Assert.Equal(expectedPropertyName, result.PropertyName);
        Assert.Equal(expectedQueryExpressionType, result.ExpressionType);
        Assert.True(equalityValueCallback.Invoke(result.PropertyValue));
    }

    private class SuccessTestData : TheoryData<Expression<Func<RawPropertiesTestModel, bool>>, string, QueryExpressionType, Func<object?, bool>>
    {
        public SuccessTestData()
        {
            Add(x => x.Prop02 > 8, "Prop02", QueryExpressionType.GreaterThan, x => x is 8);
            Add(x => x.Prop03 >= 3u, "Prop03", QueryExpressionType.GreaterOrEqualTo, x => x is 3u);
            Add(x => x.Prop06 == 0d, "Prop06", QueryExpressionType.EqualTo, x => x is 0d);
            Add(x => x.Prop07 < -7.48f, "Prop07", QueryExpressionType.LowerThan, x => x is -7.48f);
            Add(x => x.Prop02 <= 0, "Prop02", QueryExpressionType.LowerOrEqualTo, x => x is 0);
            Add(x => x.Prop10 == null, "Prop10", QueryExpressionType.Is, x => x is null);
            Add(x => x.Prop01 != null, "Prop01", QueryExpressionType.IsNot, x => x is null);
        }
    }

    [Theory]
    [ClassData(typeof(InvalidTestData))]
    public void AnalyzeQueryShouldThrowExceptionGivenInvalidExpression(Expression<Func<RawPropertiesTestModel, bool>> query)
    {
        using var scope = CreateScope();

        var sut = scope.ServiceProvider.GetRequiredService<IQueryAnalyzer>();
        Assert.Throws<InvalidLambdaSyntaxException>(() => sut.AnalyzeQuery(query));
        
    }

    private class InvalidTestData: TheoryData<Expression<Func<RawPropertiesTestModel, bool>>>
    {
        public InvalidTestData()
        {
            Add(x => x.Prop04);
            Add(x => !x.Prop11);
            Add(x => string.IsNullOrWhiteSpace(x.Prop01));
            Add(x => x.Prop10 == null ? true : false);
            Add(x => x.Prop10 > null);
            Add(x => x.Prop10 >= null);
            Add(x => x.Prop10 < null);
            Add(x => x.Prop10 <= null);
        }
    }
}