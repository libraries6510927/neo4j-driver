using System.Linq.Expressions;
using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.LambdaSyntax;
using CMSL.Databases.GDE.DotNetSyntaxInterpreting.LambdaSyntax;
using CMSL.Databases.GDE.DotNetSyntaxInterpreting.LambdaSyntax.ExpressionAnalyzers;
using CMSL.Databases.GDE.Tests.CrossCutting;
using CMSL.Databases.GDE.Tests.Unit.TestModels;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CMSL.Databases.GDE.Tests.Unit.DotNetSyntaxInterpreting.LambdaSyntax;

public class MemberExpressionAnalyzerTests: InnerDependencyInjectionBase
{

    public MemberExpressionAnalyzerTests(): base(ConfigureServices)
    {
        
    }
    
    private static void ConfigureServices(IServiceCollection serviceCollection, IConfiguration configuration)
    {
        serviceCollection
            .AddTransient<IQueryAnalyzer, QueryAnalyzer>()
            .AddSingleton<IExpressionAnalyzerFactory, ExpressionAnalyzerFactory>();
    }

    [Fact]
    public void GetValueShouldReturnNullGivenExpressionWithNullValue()
    {
        using var scope = CreateScope();
        var sut = scope.ServiceProvider.GetRequiredService<IQueryAnalyzer>();
        int? nullValue = null;
        var expression = sut.AnalyzeQuery<RawPropertiesTestModel>(x => x.Prop10 == nullValue);
        
        Assert.Equal("Prop10", expression.PropertyName);
        Assert.Null(expression.PropertyValue);
        Assert.Equal(QueryExpressionType.Is, expression.ExpressionType);
    }
}

