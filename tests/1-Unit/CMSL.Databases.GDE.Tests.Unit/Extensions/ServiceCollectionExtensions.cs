using Microsoft.Extensions.DependencyInjection;
using Moq;

namespace CMSL.Databases.GDE.Tests.Unit.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddScopedMock<T>(this IServiceCollection sc) where T:class
    {
        return sc
            .AddScoped<Mock<T>>(_ => new Mock<T>())
            .AddScoped<T>(serviceCollection => serviceCollection.GetRequiredService<Mock<T>>().Object);
    }
}