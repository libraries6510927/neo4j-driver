namespace CMSL.Databases.GDE.Tests.Unit.TestModels;

public record RawPropertiesTestModel
{
    public string Prop01 { get; set; } = string.Empty;
    public int Prop02 { get; set; }
    public uint Prop03 { get; set; }
    public bool Prop04 { get; set; }
    public char Prop05 { get; set; }
    public double Prop06 { get; set; }
    public float Prop07 { get; set; }
    public decimal Prop08 { get; set; }
    public Guid Prop09 { get; set; }
    public int? Prop10 { get; set; } = null;
    public bool Prop11 { get; set; }
    public string Prop12 { get; set; } = string.Empty;
    public string Prop13 { get; set; } = string.Empty;
    public Guid? Prop14 { get; set; } = Guid.Empty;
}