namespace CMSL.Databases.GDE.Tests.Unit.TestModels;

public class OnlyOneAllowedPropertyTestModel
{
    public string Allowed { get; set; } = string.Empty;
    private string _notAllowed01 = string.Empty;
    public string NotAllowed02 { get; } = string.Empty;
    private string NotAllowed03 { get; } = string.Empty;
    public string NotAllowed04 { get; private set; } = string.Empty;
    public string NotAllowed05 { get; protected set; } = string.Empty;
    public string NotAllowed06 { get; init; } = string.Empty;
    public string NotAllowed07 => Guid.NewGuid().ToString();
}