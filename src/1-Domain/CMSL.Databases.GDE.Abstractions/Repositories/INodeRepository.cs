namespace CMSL.Databases.GDE.Abstractions.Repositories;

public interface INodeRepository<T>: IQueryableNodeRepository<T> where T: class, new()
{
    Task UpdateAsync();
    Task DeleteAsync();
    Task Add(T model);
}