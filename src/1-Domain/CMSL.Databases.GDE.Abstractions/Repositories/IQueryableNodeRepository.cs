using System.Linq.Expressions;

namespace CMSL.Databases.GDE.Abstractions.Repositories;

public interface IQueryableNodeRepository<T> where T: class, new()
{
    INodeRepository<T> Where(Expression<Func<T, bool>> condition);
    Task<List<T>> ToListAsync();
    Task<T> SingleAsync();
}