using CMSL.Databases.GDE.Abstractions.Repositories;

namespace CMSL.Databases.GDE.Abstractions;

public interface IGraphDatabaseContext
{
    INodeRepository<T> GetNodesRepository<T>() where T : class, new();
    IEdgeRepository<T> GetEdgesRepository<T>() where T : class, new();
}