using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.LambdaSyntax;
using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.ObjectPropertiesExplorer;

namespace CMSL.Databases.GDE.Abstractions.QueryBuilding;

public interface IQueryBuilder
{
    Query BuildForAdding(PropertyValueCollection properties, ModelType modelType);
    Query BuildForQuerying(PropertyExpressionCollection properties, ModelType modelType);
    Query BuildForDeleting(PropertyExpressionCollection properties, ModelType modelType);
    Query BuildForUpdating(PropertyValueCollection properties, ModelType modelType);
}