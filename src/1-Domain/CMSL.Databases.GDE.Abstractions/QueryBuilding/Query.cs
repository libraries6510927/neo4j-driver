namespace CMSL.Databases.GDE.Abstractions.QueryBuilding;

public class Query
{
    public string QueryString { get; }

    public Query(string query)
    {
        QueryString = query;
    }

    public override string ToString() => QueryString;
    public static implicit operator string(Query q) => q.QueryString;
}