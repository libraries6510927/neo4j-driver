using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.LambdaSyntax;
using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.ObjectPropertiesExplorer;

namespace CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting;

/// <summary>
/// Evaluates the properties as key-value pairs for Neo4j queries
/// </summary>
public interface IQueryDtoEvaluator
{
    PropertyValueCollection Evaluate(ModelProperties modelProperties);
    PropertyExpressionCollection Evaluate(QueryExpressions queries);
}