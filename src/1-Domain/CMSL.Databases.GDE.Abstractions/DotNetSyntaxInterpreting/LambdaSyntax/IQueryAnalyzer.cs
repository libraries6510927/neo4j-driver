using System.Linq.Expressions;

namespace CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.LambdaSyntax;

public interface IQueryAnalyzer
{
    QueryExpression AnalyzeQuery<T>(Expression<Func<T, bool>> predicate);
}