using System.Collections;
using System.Collections.ObjectModel;

namespace CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.LambdaSyntax;

public class PropertyExpressionCollection: IEnumerable<PropertyExpression>
{
    private readonly List<PropertyExpression> _expressions = new List<PropertyExpression>();

    public IReadOnlyCollection<PropertyExpression> Expressions => new ReadOnlyCollection<PropertyExpression>(_expressions);
    public PropertyExpressionCollection(IEnumerable<PropertyExpression> expressions)
    {
        _expressions.AddRange(expressions);
    }


    public IEnumerator<PropertyExpression> GetEnumerator() => Expressions.GetEnumerator();
    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public string SerializedWithSelector(char selector)
    {
        var propertiesWithSelector = this
            .Select(x => $"{selector}.{x.PropertyName} {x.ExpressionOperator} {x.PropertyValue}");

        return string.Join(" AND ", propertiesWithSelector);
    }
}