using System.Collections;
using System.Collections.ObjectModel;

namespace CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.LambdaSyntax;

public class QueryExpressions: IEnumerable<QueryExpression>
{
    private readonly List<QueryExpression> _queries = new();

    public IReadOnlyCollection<QueryExpression> Queries => new ReadOnlyCollection<QueryExpression>(_queries);
    public QueryExpressions()
    {
        
    }
    
    public QueryExpressions(IEnumerable<QueryExpression>queries)
    {
        _queries.AddRange(queries);
    }

    public void Add(QueryExpression query)
    {
        _queries.Add(query);
    }
    
    public IEnumerator<QueryExpression> GetEnumerator() => _queries.GetEnumerator();
    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}