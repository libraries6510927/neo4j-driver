using System.Linq.Expressions;

namespace CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.LambdaSyntax;

public interface IExpressionAnalyzerFactory
{
    IExpressionAnalyzer GetExpressionAnalyzer(Expression expression);
}