namespace CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.LambdaSyntax;

public class PropertyExpression
{
    public string PropertyName { get; }
    public string PropertyValue { get;  }
    public string ExpressionOperator { get; }
    
    public PropertyExpression(string propertyName, string propertyValue, string expressionOperator)
    {
        PropertyName = propertyName;
        PropertyValue = propertyValue;
        ExpressionOperator = expressionOperator;
    }
}