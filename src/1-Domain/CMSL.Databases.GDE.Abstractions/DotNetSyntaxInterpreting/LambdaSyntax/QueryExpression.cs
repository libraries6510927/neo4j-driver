namespace CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.LambdaSyntax;

public class QueryExpression
{
    public string PropertyName { get; }
    public object? PropertyValue { get; }
    public QueryExpressionType ExpressionType { get; }

    public QueryExpression(QueryExpressionType expressionType, string propertyName, object? propertyValue = null)
    {
        PropertyName = propertyName;
        PropertyValue = propertyValue;
        ExpressionType = expressionType;
    }
}