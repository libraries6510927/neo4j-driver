namespace CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.LambdaSyntax;

public enum QueryExpressionType
{
    EqualTo,
    LowerThan,
    GreaterThan,
    DifferentOf,
    LowerOrEqualTo,
    GreaterOrEqualTo,
    Is,
    IsNot
}