namespace CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.LambdaSyntax;

public interface IExpressionAnalyzer
{
    object? GetValue();
}