using System.Collections;

namespace CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.ObjectPropertiesExplorer;

public class PropertyValueCollection: IEnumerable<KeyValuePair<string, string>>
{
    private readonly Dictionary<string, string> _propertyValuePairs;

    public int Count { get; }
    public PropertyValueCollection(Dictionary<string, string> propertyValuePairs)
    {
        _propertyValuePairs = propertyValuePairs;
        Count = _propertyValuePairs.Count;
    }

    public bool ContainsKey(string key) => _propertyValuePairs.ContainsKey(key);
    public string this[string key] => _propertyValuePairs[key];
    public IEnumerator<KeyValuePair<string, string>> GetEnumerator() => _propertyValuePairs.GetEnumerator();
    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}