using System.Reflection;

namespace CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.ObjectPropertiesExplorer;

/// <summary>
/// Gets the class' properties available for mapping
/// </summary>
public interface IPropertyExplorer
{
    /// <summary>
    /// Gets all public properties with getters and setters
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns>An enumerable white those properties</returns>
    ModelProperties GetProperties<T>(T model) where T : class;

    IEnumerable<PropertyInfo> GetPropertiesFromClass<T>() where T : class;
    IEnumerable<PropertyInfo> GetPropertiesFromClass(Type type);
}