using System.Collections.ObjectModel;
using System.Reflection;

namespace CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.ObjectPropertiesExplorer;

public class ModelProperties
{
    private readonly List<PropertyInfo> _propertyInfos = new();
    public IReadOnlyCollection<PropertyInfo> PropertyInfos => new ReadOnlyCollection<PropertyInfo>(_propertyInfos);
    public object Instance { get;  }

    public ModelProperties(IEnumerable<PropertyInfo> propertyInfos, object instance)
    {
        _propertyInfos.AddRange(propertyInfos);
        Instance = instance;
    }
}