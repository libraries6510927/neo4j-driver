namespace CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.ObjectPropertiesExplorer;

public interface IDatabasePropertiesBinder
{
    T BindTo<T>(IReadOnlyDictionary<string, object?> databaseProperties) where T: class, new() ;
}