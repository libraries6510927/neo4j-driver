namespace CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.ObjectPropertiesExplorer;

public interface ITypeIdentifier
{
    ModelType GetModelType<T>() where T : class;
}