namespace CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.ObjectPropertiesExplorer;

public class ModelType
{
    public string Type { get; }

    public ModelType(string type)
    {
        Type = type;
    }
    
    public static implicit operator string(ModelType t) => t.Type;
    public override string ToString() => Type;
}