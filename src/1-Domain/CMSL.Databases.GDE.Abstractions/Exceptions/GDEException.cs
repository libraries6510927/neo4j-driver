namespace CMSL.Databases.GDE.Abstractions.Exceptions;

public class GdeException: Exception
{
    public GdeException(string message, Exception? innerException = null): base(message, innerException)
    {
        
    }
}