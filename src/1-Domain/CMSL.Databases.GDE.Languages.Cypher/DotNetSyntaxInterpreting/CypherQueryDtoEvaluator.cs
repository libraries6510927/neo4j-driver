using System.Globalization;
using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting;
using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.LambdaSyntax;
using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.ObjectPropertiesExplorer;

namespace CMSL.Databases.GDE.Languages.Cypher.DotNetSyntaxInterpreting;

internal class CypherQueryDtoEvaluator: IQueryDtoEvaluator
{
    public PropertyValueCollection Evaluate(ModelProperties modelProperties)
    {
        var pairs = modelProperties.PropertyInfos
            .Select(p => (name: p.Name, value: p.GetValue(modelProperties.Instance)))
            .Select(x => (name: x.name, value: EvaluateValue(x.value)))
            .ToDictionary(x => x.name, x => x.value);

        return new PropertyValueCollection(pairs);
    }

    public PropertyExpressionCollection Evaluate(QueryExpressions queries)
    {
        var expressions = queries
            .Select(x => new PropertyExpression(x.PropertyName, EvaluateValue(x.PropertyValue), EvaluateExpressionType(x.ExpressionType)));

        return new PropertyExpressionCollection(expressions);
    }

    private string EvaluateValue(object? value)
    {
        if (value is null) return "NULL";
        if (value is string or char or Guid) return  $"'{value}'";
        if (value is bool boolValue) return boolValue ? "true" : "false";
        if (value is float floatValue) return floatValue.ToString(CultureInfo.InvariantCulture); 
        if (value is decimal decimalValue) return decimalValue.ToString(CultureInfo.InvariantCulture); 
        if (value is double doubleValue) return doubleValue.ToString(CultureInfo.InvariantCulture); 
        return $"{value}";
    }

    private string EvaluateExpressionType(QueryExpressionType expressionType) => expressionType switch
    {
        QueryExpressionType.DifferentOf => "<>",
        QueryExpressionType.EqualTo => "=",
        QueryExpressionType.GreaterThan => ">",
        QueryExpressionType.LowerThan => "<",
        QueryExpressionType.GreaterOrEqualTo => ">=",
        QueryExpressionType.LowerOrEqualTo => "<=",
        QueryExpressionType.Is => "IS",
        QueryExpressionType.IsNot => "IS NOT",
        _ => throw new InvalidOperationException($"The expression type {expressionType} is not supported. Contact the developers, this error wasn't suppose to happen")
    };
}