using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting;
using CMSL.Databases.GDE.Abstractions.QueryBuilding;
using CMSL.Databases.GDE.Languages.Cypher.DotNetSyntaxInterpreting;
using CMSL.Databases.GDE.Languages.Cypher.QueryBuilding;
using Microsoft.Extensions.DependencyInjection;

namespace CMSL.Databases.GDE.Languages.Cypher;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddCypherGraphDatabaseBase(this IServiceCollection serviceCollection)
    {
        return serviceCollection
            .AddGraphDatabaseBase()
            .AddSingleton<IQueryDtoEvaluator, CypherQueryDtoEvaluator>()
            .AddSingleton<IQueryBuilder, CypherQueryBuilder>();
    }
}