using System.Text;
using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.LambdaSyntax;
using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.ObjectPropertiesExplorer;
using CMSL.Databases.GDE.Abstractions.QueryBuilding;

namespace CMSL.Databases.GDE.Languages.Cypher.QueryBuilding;

internal class CypherQueryBuilder: IQueryBuilder
{
    public Query BuildForAdding(PropertyValueCollection properties, ModelType modelType)
    {
        var serializedProperties = SerializeForAdding(properties);
        return new Query($"CREATE(:{modelType} {{{serializedProperties}}})");
    }
    
    private static string SerializeForAdding(PropertyValueCollection properties)
    {
        return string.Join(", ", properties
            .Select(x => $"{x.Key}: {x.Value}"));
    }

    public Query BuildForQuerying(PropertyExpressionCollection properties, ModelType modelType)
    {
        const char selector = 'x';
        var rawQuery =  new StringBuilder()
            .Append(MatchWithWhere(selector, modelType, properties))
            .Append($"RETURN {selector}")
            .ToString();

        return new Query(rawQuery);
    }

    public Query BuildForDeleting(PropertyExpressionCollection properties, ModelType modelType)
    {
        const char selector = 'x';
        var rawQuery = new StringBuilder()
            .Append(MatchWithWhere(selector, modelType, properties))
            .Append($"DELETE {selector}")
            .ToString();

        return new Query(rawQuery);
    }

    private static string MatchWithWhere(char selector, ModelType modelType, PropertyExpressionCollection properties)
    {
        return new StringBuilder()
            .Append($"MATCH ({selector}:{modelType})")
            .Append('\n')
            .Append($"WHERE {properties.SerializedWithSelector(selector)}")
            .Append('\n')
            .ToString();
    }
 
    public Query BuildForUpdating(PropertyValueCollection properties, ModelType modelType)
    {
        throw new NotImplementedException();
    }
}