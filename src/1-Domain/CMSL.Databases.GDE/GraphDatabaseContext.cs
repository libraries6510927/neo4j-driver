using CMSL.Databases.GDE.Abstractions;
using CMSL.Databases.GDE.Abstractions.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace CMSL.Databases.GDE;

internal class GraphDatabaseContext: IGraphDatabaseContext
{
    private readonly IServiceProvider _serviceProvider;

    public GraphDatabaseContext(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public INodeRepository<T> GetNodesRepository<T>() where T : class, new()
    {
        return _serviceProvider.GetRequiredService<INodeRepository<T>>();
    }

    public IEdgeRepository<T> GetEdgesRepository<T>() where T : class, new()
    {
        throw new NotImplementedException();
    }
}