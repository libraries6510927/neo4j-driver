using System.Linq.Expressions;
using CMSL.Databases.GDE.Abstractions.Repositories;
using Microsoft.Extensions.DependencyInjection;
using CMSL.Databases.GDE;
using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting;
using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.LambdaSyntax;
using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.ObjectPropertiesExplorer;
using CMSL.Databases.GDE.Abstractions.Exceptions;
using CMSL.Databases.GDE.Abstractions.QueryBuilding;

namespace CMSL.Databases.GDE.Repositories;

internal class NodeRepository<T> : INodeRepository<T> where T: class, new()
{
    private readonly IServiceProvider _serviceProvider;
    private readonly IPropertyExplorer _propertyExplorer;
    private readonly IQueryDtoEvaluator _queryDtoEvaluator;
    private readonly IQueryBuilder _queryBuilder;
    private readonly ITypeIdentifier _typeIdentifier;
    private readonly IGraphDatabaseDriver _graphDatabaseDriver;
    private readonly IDatabasePropertiesBinder _databasePropertiesBinder;

    private readonly List<Expression<Func<T, bool>>> _filters = new();

    public NodeRepository(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
        _propertyExplorer = serviceProvider.GetRequiredService<IPropertyExplorer>();
        _queryDtoEvaluator = serviceProvider.GetRequiredService<IQueryDtoEvaluator>();
        _queryBuilder = serviceProvider.GetRequiredService<IQueryBuilder>();
        _typeIdentifier = serviceProvider.GetRequiredService<ITypeIdentifier>();
        _databasePropertiesBinder = serviceProvider.GetRequiredService<IDatabasePropertiesBinder>();

        _graphDatabaseDriver = serviceProvider.GetRequiredService<IGraphDatabaseDriver>();
    }

    public async Task Add(T model)
    {
        var properties = _propertyExplorer.GetProperties(model);
        var propertiesValues = _queryDtoEvaluator.Evaluate(properties);
        var modelType = _typeIdentifier.GetModelType<T>();

        var query = _queryBuilder.BuildForAdding(propertiesValues, modelType);
        await _graphDatabaseDriver.ExecuteQueryWithoutReturnAsync(query);
    }

    public Task UpdateAsync()
    {
        throw new NotImplementedException();
    }

    public async Task DeleteAsync()
    {
        if (!_filters.Any()) throw new GdeException("Trying to delete the whole database because you didn't provide any filters. If you want this, use the CMSL.Databases.DatabaseCleaner library");
        var expressions = await GetExpressionsFromFilters();
        var modelType = _typeIdentifier.GetModelType<T>();

        var query = _queryBuilder.BuildForDeleting(expressions, modelType);
        await _graphDatabaseDriver.ExecuteQueryWithoutReturnAsync(query);
    }

    public INodeRepository<T> Where(Expression<Func<T, bool>> condition)
    {
        _filters.Add(condition);
        return this;
    }

    public async Task<List<T>> ToListAsync()
    {
        var models = await GetRecordsFromDatabase();
        return models.ToList();
    }

    public async Task<T> SingleAsync()
    {
        var models = await GetRecordsFromDatabase();
        return models.Single();
    }

    private async Task<T[]> GetRecordsFromDatabase()
    {
        var expressions = await GetExpressionsFromFilters();
        var query = _queryBuilder.BuildForQuerying(expressions, _typeIdentifier.GetModelType<T>());


        var nodes = await _graphDatabaseDriver.ExecuteQueryWithReturnAsync(query);
        var modelsTasks = nodes
            .Select(x => Task.Run(() => _databasePropertiesBinder.BindTo<T>(x)))
            .ToArray();
        return await Task.WhenAll(modelsTasks);
    }

    private async Task<PropertyExpressionCollection> GetExpressionsFromFilters()
    {
        var filterTasks = _filters
            .Select(x => Task.Run(() =>
            {
                var queryAnalyzer = _serviceProvider.GetRequiredService<IQueryAnalyzer>();
                return queryAnalyzer.AnalyzeQuery(x);
            }))
            .ToArray();

        var filters = new QueryExpressions(await Task.WhenAll(filterTasks));
        var evaluatedFilters = _queryDtoEvaluator.Evaluate(filters);
        
        return evaluatedFilters;
    }
}