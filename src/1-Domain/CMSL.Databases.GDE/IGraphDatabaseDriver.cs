using CMSL.Databases.GDE.Abstractions.QueryBuilding;

namespace CMSL.Databases.GDE;

public interface IGraphDatabaseDriver
{
    Task ExecuteQueryWithoutReturnAsync(Query query);
    Task<IReadOnlyCollection<IReadOnlyDictionary<string, object>>> ExecuteQueryWithReturnAsync(Query query);
}