using CMSL.Databases.GDE.Repositories;
using CMSL.Databases.GDE.Abstractions;
using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.LambdaSyntax;
using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.ObjectPropertiesExplorer;
using CMSL.Databases.GDE.Abstractions.Repositories;
using CMSL.Databases.GDE.DotNetSyntaxInterpreting.LambdaSyntax;
using CMSL.Databases.GDE.DotNetSyntaxInterpreting.LambdaSyntax.ExpressionAnalyzers;
using CMSL.Databases.GDE.DotNetSyntaxInterpreting.ObjectPropertiesExplorer;
using Microsoft.Extensions.DependencyInjection;

namespace CMSL.Databases.GDE;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddGraphDatabaseBase(this IServiceCollection serviceCollection)
    {
        return serviceCollection
            .AddScoped<IGraphDatabaseContext, GraphDatabaseContext>()
            .AddTransient(typeof(INodeRepository<>), typeof(NodeRepository<>))
            .AddSingleton<IPropertyExplorer, PropertyExplorer>()
            .AddSingleton<ITypeIdentifier, TypeIdentifier>()
            .AddSingleton<IDatabasePropertiesBinder, DatabasePropertiesBinder>()
            .AddTransient<IQueryAnalyzer, QueryAnalyzer>()
            .AddSingleton<IExpressionAnalyzerFactory, ExpressionAnalyzerFactory>();
    }
}