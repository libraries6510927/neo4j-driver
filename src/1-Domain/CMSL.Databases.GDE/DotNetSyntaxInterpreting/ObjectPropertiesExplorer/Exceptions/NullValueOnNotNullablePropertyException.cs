using CMSL.Databases.GDE.Abstractions.Exceptions;

namespace CMSL.Databases.GDE.DotNetSyntaxInterpreting.ObjectPropertiesExplorer.Exceptions;

public class NullValueOnNotNullablePropertyException: GdeException
{
    private static readonly string CustomMessagem = "The property {0}, of type {1}, does not accept null values";

    public NullValueOnNotNullablePropertyException(string propertyName, Type propertyType, Exception? innerException = null) 
        : base(string.Format(CustomMessagem, propertyName, propertyType.FullName), innerException)
    {
    }
}