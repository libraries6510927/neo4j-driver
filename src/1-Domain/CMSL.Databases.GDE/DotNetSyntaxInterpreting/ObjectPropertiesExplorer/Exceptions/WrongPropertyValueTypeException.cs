using CMSL.Databases.GDE.Abstractions.Exceptions;

namespace CMSL.Databases.GDE.DotNetSyntaxInterpreting.ObjectPropertiesExplorer.Exceptions;

public class WrongPropertyValueTypeException: GdeException
{
    private static readonly string CustomMessage = "The property {0} has value type of {1}. Type {2} was provided instead.";
    
    public WrongPropertyValueTypeException(string propertyName, Type propertyType, Type providedType, Exception? innerException = null) 
        : base(string.Format(CustomMessage,propertyName, propertyType.FullName, providedType.FullName), innerException)
    {
    }
}