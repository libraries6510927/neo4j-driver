using System.Reflection;
using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.ObjectPropertiesExplorer;

namespace CMSL.Databases.GDE.DotNetSyntaxInterpreting.ObjectPropertiesExplorer;

internal class PropertyExplorer: IPropertyExplorer
{
    public ModelProperties GetProperties<T>(T model) where T : class => new ModelProperties(GetPropertiesFromClass(model.GetType()), model);
    public IEnumerable<PropertyInfo> GetPropertiesFromClass<T>() where T : class => GetPropertiesFromClass(typeof(T));

    public IEnumerable<PropertyInfo> GetPropertiesFromClass(Type type)
    {
        return type
            .GetProperties(BindingFlags.Public | BindingFlags.Instance)
            .Where(p => p is { CanRead: true, CanWrite: true })
            .Where(p => FilterSetMethod(p.GetSetMethod()));
    }

    private bool FilterSetMethod(MethodInfo? setMethod)
    {
        if (setMethod is null) return false;
        if (!setMethod.IsPublic) return false;

        var setMethodModifiers = setMethod.ReturnParameter.GetRequiredCustomModifiers();
        var isInitOnly = setMethodModifiers.Contains(typeof(System.Runtime.CompilerServices.IsExternalInit));

        return !isInitOnly;
    }
}