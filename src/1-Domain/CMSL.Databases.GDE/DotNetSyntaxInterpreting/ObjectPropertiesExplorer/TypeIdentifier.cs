using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.ObjectPropertiesExplorer;

namespace CMSL.Databases.GDE.DotNetSyntaxInterpreting.ObjectPropertiesExplorer;

internal class TypeIdentifier: ITypeIdentifier
{
    public ModelType GetModelType<T>() where T : class
    {
        return new ModelType(typeof(T).Name);
    }
}