using System.Reflection;
using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.ObjectPropertiesExplorer;
using CMSL.Databases.GDE.DotNetSyntaxInterpreting.ObjectPropertiesExplorer.Exceptions;
using Guid = System.Guid;

namespace CMSL.Databases.GDE.DotNetSyntaxInterpreting.ObjectPropertiesExplorer;

internal class DatabasePropertiesBinder: IDatabasePropertiesBinder
{
    private readonly IPropertyExplorer _propertyExplorer;

    public DatabasePropertiesBinder(IPropertyExplorer propertyExplorer)
    {
        _propertyExplorer = propertyExplorer;
    }

    public T BindTo<T>(IReadOnlyDictionary<string, object?> databaseProperties) where T : class, new()
    {
        var suitableProperties = _propertyExplorer
            .GetPropertiesFromClass<T>()
            .ToDictionary(x => x.Name, x => x);
        
        var model = new T();
        foreach (var property in databaseProperties)
        {
            if (!suitableProperties.ContainsKey(property.Key)) continue;
            var modelProperty = suitableProperties[property.Key];
            
            if (property.Value is null)  ExecuteNullLogic(modelProperty, property, model);
            else ExecuteNotNullLogic(modelProperty, property, model);
        }

        return model;
    }

    private void ExecuteNotNullLogic<T>(PropertyInfo modelProperty, KeyValuePair<string, object?> property, T model) where T : class, new()
    {
        var typeToBeAnalyzed = CheckIfPropertyIsNullable(modelProperty) ? GetNullableTypeArgument(modelProperty) : modelProperty.PropertyType;
        var analyzedValue = AnalyzeForAnySpecialType(typeToBeAnalyzed, property.Value!);
        if (!modelProperty.PropertyType.IsInstanceOfType(analyzedValue)) throw new WrongPropertyValueTypeException(property.Key, modelProperty.PropertyType, analyzedValue.GetType());

        modelProperty.SetValue(model, analyzedValue);
    }

    private void ExecuteNullLogic<T>(PropertyInfo modelProperty, KeyValuePair<string, object?> property, T model) where T : class, new()
    {
        var isNullable = CheckIfPropertyIsNullable(modelProperty);
        if (!isNullable) throw new NullValueOnNotNullablePropertyException(modelProperty.Name, modelProperty.PropertyType);
        var genericArgument = GetNullableTypeArgument(modelProperty);

        var analyzedValue = property.Value is null ? null : AnalyzeForAnySpecialType(genericArgument, property.Value);
        modelProperty.SetValue(model, analyzedValue);
    }

    private static Type GetNullableTypeArgument(PropertyInfo modelProperty)
    {
        var genericArgument = modelProperty.PropertyType.GetGenericArguments().Single();
        return genericArgument;
    }

    private static bool CheckIfPropertyIsNullable(PropertyInfo modelProperty)
    {
        var isNullable = modelProperty.PropertyType.IsGenericType && modelProperty.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>);
        return isNullable;
    }

    private object AnalyzeForAnySpecialType(Type modelPropertyPropertyType, object propertyValue)
    {
        if (modelPropertyPropertyType == typeof(Guid)) return Guid.Parse(propertyValue.ToString());
        if (modelPropertyPropertyType == typeof(int) && propertyValue is long and >= int.MinValue and <= int.MaxValue) return Convert.ToInt32(propertyValue);

        return propertyValue;
    }
}