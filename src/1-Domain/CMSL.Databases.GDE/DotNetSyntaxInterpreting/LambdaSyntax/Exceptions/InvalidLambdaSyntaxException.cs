using CMSL.Databases.GDE.Abstractions.Exceptions;

namespace CMSL.Databases.GDE.DotNetSyntaxInterpreting.LambdaSyntax.Exceptions;

public class InvalidLambdaSyntaxException: GdeException
{
    private static readonly string GenericMessage = "Your lambda function is invalid";
    private static readonly string DetailedMessage = "Your lambda function is invalid: {0}";
    public InvalidLambdaSyntaxException(string details, Exception? innerException = null)
        : base(string.Format(DetailedMessage, details), innerException)
    {
        
    }
    public InvalidLambdaSyntaxException(Exception? innerException = null): base(GenericMessage, innerException)
    {
    }
}