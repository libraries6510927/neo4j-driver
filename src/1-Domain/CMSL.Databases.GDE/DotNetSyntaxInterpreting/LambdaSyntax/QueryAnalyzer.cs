using System.Linq.Expressions;
using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.LambdaSyntax;
using CMSL.Databases.GDE.DotNetSyntaxInterpreting.LambdaSyntax.Exceptions;

namespace CMSL.Databases.GDE.DotNetSyntaxInterpreting.LambdaSyntax;

internal class QueryAnalyzer: ExpressionVisitor, IQueryAnalyzer
{
    private string _propertyName = string.Empty;
    private object? _propertyValue = null;
    private QueryExpressionType _expressionType;

    private readonly IExpressionAnalyzerFactory _expressionAnalyzerFactory;

    public QueryAnalyzer(IExpressionAnalyzerFactory expressionAnalyzerFactory)
    {
        _expressionAnalyzerFactory = expressionAnalyzerFactory;
    }

    public QueryExpression AnalyzeQuery<T>(Expression<Func<T, bool>> predicate)
    {
        if (predicate.Body is not BinaryExpression) throw new InvalidLambdaSyntaxException($"The lambda is not a binary expression -> {predicate.GetType().Name}");
        Visit(predicate);
        return new QueryExpression(_expressionType, _propertyName, _propertyValue);
    }

    protected override Expression VisitBinary(BinaryExpression node)
    {
        if(node.Left is not MemberExpression memberExpression) throw new InvalidLambdaSyntaxException($"The left node is not a MemberExpression -> {node.Left.GetType().Name}");
        var expressionAnalyzer = _expressionAnalyzerFactory.GetExpressionAnalyzer(node.Right);
        
        _propertyName = memberExpression.Member.Name;
        _propertyValue = expressionAnalyzer.GetValue();
        SetExpressionType(node.NodeType);

        return base.VisitBinary(node);
    }
    
    private void SetExpressionType(ExpressionType nodeType)
    {
        if (_propertyValue is null)
        {
            _expressionType = nodeType switch
            {
                ExpressionType.Equal => QueryExpressionType.Is,
                ExpressionType.NotEqual => QueryExpressionType.IsNot,
                _ => throw new InvalidLambdaSyntaxException("The only supported null expressions is with '==' and '!='")
            };
        }
        else
        {
            _expressionType = nodeType switch
            {
                ExpressionType.Equal => QueryExpressionType.EqualTo,
                ExpressionType.NotEqual => QueryExpressionType.DifferentOf,
                ExpressionType.GreaterThan => QueryExpressionType.GreaterThan,
                ExpressionType.GreaterThanOrEqual => QueryExpressionType.GreaterOrEqualTo,
                ExpressionType.LessThan => QueryExpressionType.LowerThan,
                ExpressionType.LessThanOrEqual => QueryExpressionType.LowerOrEqualTo,
                _ => throw new InvalidLambdaSyntaxException($"The binary expression contains an invalid node type -> {nodeType}")
            };
        }    
    }
}