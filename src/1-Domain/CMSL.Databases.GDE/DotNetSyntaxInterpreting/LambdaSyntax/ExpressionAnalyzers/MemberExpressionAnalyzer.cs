using System.Linq.Expressions;
using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.LambdaSyntax;

namespace CMSL.Databases.GDE.DotNetSyntaxInterpreting.LambdaSyntax.ExpressionAnalyzers;

internal class MemberExpressionAnalyzer: IExpressionAnalyzer
{
    private readonly MemberExpression _memberExpression;

    public MemberExpressionAnalyzer(MemberExpression memberExpression)
    {
        _memberExpression = memberExpression;
    }

    public object? GetValue()
    {
        var objectMember = Expression.Convert(_memberExpression, typeof(object));
        var getterLambda = Expression.Lambda<Func<object?>>(objectMember);
        var getter = getterLambda.Compile();

        return getter();
    }
}