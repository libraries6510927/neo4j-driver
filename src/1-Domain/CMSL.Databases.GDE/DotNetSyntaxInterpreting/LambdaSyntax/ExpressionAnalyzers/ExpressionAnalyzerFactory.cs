using System.Linq.Expressions;
using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.LambdaSyntax;
using CMSL.Databases.GDE.DotNetSyntaxInterpreting.LambdaSyntax.Exceptions;

namespace CMSL.Databases.GDE.DotNetSyntaxInterpreting.LambdaSyntax.ExpressionAnalyzers;

internal class ExpressionAnalyzerFactory: IExpressionAnalyzerFactory
{
    public IExpressionAnalyzer GetExpressionAnalyzer(Expression expression)
    {
        if (expression is ConstantExpression constantExpression) return new ConstantExpressionAnalyzer(constantExpression);
        if (expression is MemberExpression memberExpression) return new MemberExpressionAnalyzer(memberExpression);
        
        throw new InvalidLambdaSyntaxException($"The expression {expression.GetType().Name} is not supported");
    }
}