using System.Linq.Expressions;
using CMSL.Databases.GDE.Abstractions.DotNetSyntaxInterpreting.LambdaSyntax;
using CMSL.Databases.GDE.DotNetSyntaxInterpreting.LambdaSyntax.Exceptions;

namespace CMSL.Databases.GDE.DotNetSyntaxInterpreting.LambdaSyntax.ExpressionAnalyzers;

internal class ConstantExpressionAnalyzer: IExpressionAnalyzer
{
    private readonly ConstantExpression _expression;

    public ConstantExpressionAnalyzer(ConstantExpression expression)
    {
        _expression = expression;
    }

    public object? GetValue() => _expression.Value;
    
}