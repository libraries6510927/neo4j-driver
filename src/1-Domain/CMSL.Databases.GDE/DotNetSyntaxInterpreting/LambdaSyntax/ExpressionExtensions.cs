using System.Linq.Expressions;
using System.Reflection;
using CMSL.Databases.GDE.DotNetSyntaxInterpreting.LambdaSyntax.Exceptions;

namespace CMSL.Databases.GDE.DotNetSyntaxInterpreting.LambdaSyntax;

internal static class ExpressionExtensions
{
    public static PropertyInfo GetProperty<T, TProperty>(this Expression<Func<T, TProperty>> propertyExpression)
    {
       return propertyExpression.Body is MemberExpression {Member: PropertyInfo propertyInfo} 
           ?  propertyInfo
           : throw new InvalidLambdaSyntaxException($"The lambda is not a MemberExpression returning a PropertyInfo -> The lambda is a {propertyExpression.GetType().Name}");
    }
}