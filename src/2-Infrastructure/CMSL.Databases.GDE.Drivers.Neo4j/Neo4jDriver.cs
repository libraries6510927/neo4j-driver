using System.Collections.ObjectModel;
using CMSL.Databases.GDE;
using Neo4j.Driver;
using Query = CMSL.Databases.GDE.Abstractions.QueryBuilding.Query;

namespace CMSL.Databases.GDE.Drivers.Neo4j;

internal class Neo4jDriver: IGraphDatabaseDriver
{
    private readonly IDriver _driver;

    public Neo4jDriver(IDriver driver)
    {
        _driver = driver;
    }

    public async Task ExecuteQueryWithoutReturnAsync(Query query)
    {
        await using var session = _driver.AsyncSession(); 
        await session.ExecuteWriteAsync(async queryRunner =>
        {
            await queryRunner.RunAsync(query);
        });    
    }

    public async Task<IReadOnlyCollection<IReadOnlyDictionary<string, object>>> ExecuteQueryWithReturnAsync(Query query)
    {
        await using var session = _driver.AsyncSession(); 
        return await session.ExecuteReadAsync(async queryRunner =>
        {
            var result = await queryRunner.RunAsync(query);
            var properties =  result
                .Select(x => x.Values)
                .ToEnumerable()
                .SelectMany<IReadOnlyDictionary<string, object>, object>(x => x.Values)
                .Select(x => x as IEntity)
                .Where(x => x is not null)
                .Select(x => x.Properties)
                .ToList();

            return new ReadOnlyCollection<IReadOnlyDictionary<string, object>>(properties);
        });
    }
}