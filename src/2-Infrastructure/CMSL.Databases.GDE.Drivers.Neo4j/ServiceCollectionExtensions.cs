using CMSL.Databases.GDE;
using CMSL.Databases.GDE.Languages.Cypher;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Neo4j.Driver;

namespace CMSL.Databases.GDE.Drivers.Neo4j;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddNeo4j(this IServiceCollection serviceCollection, IConfiguration configuration, string configurationSection = "Neo4j")
    {
        return serviceCollection
            .Configure<Neo4jConfiguration>(configuration.GetSection(configurationSection))
            .AddCypherGraphDatabaseBase()
            .AddScoped<IGraphDatabaseDriver, Neo4jDriver>()
            .AddScoped<IDriver>(sp =>
            {
                var config = sp.GetRequiredService<IOptions<Neo4jConfiguration>>().Value;
                var uri = new Uri(config.Uri);
                return GraphDatabase.Driver(uri, AuthTokens.Basic(config.UserName, config.Password));
            });
    }
}